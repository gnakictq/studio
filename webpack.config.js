const webpack = require("webpack");
const webpackLiveReload = require("webpack-livereload-plugin");
const webpackTerser = require("terser-webpack-plugin");
const webpackCopy = require("copy-webpack-plugin");
const webpackReplaceHash = require("replace-hash-in-file-webpack-plugin");
const path = require("path");
const banner = require("./tasks/banner/banner.js");
const package = require("./package.json");
const production = process.argv.includes("production");
const analyzer = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

const app = {
    entry: "./src/app/index.ts",
    output: {
        filename: "app-[hash].js",
        path: path.resolve(__dirname, "static", "js"),
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig-app.json",
                },
            },
            {
                test: /\.(graphql|gql)$/,
                exclude: /node_modules/,
                loader: "graphql-tag/loader",
            },
            {
                test: /\.svg$/,
                use: [
                    "url-loader",
                    {
                        loader: "image-webpack-loader",
                        options: {
                            svgo: {
                                plugins: [
                                    { cleanupAttrs: true },
                                    { removeDoctype: true },
                                    { removeXMLProcInst: true },
                                    { removeComments: true },
                                    { removeMetadata: true },
                                    { removeTitle: true },
                                    {
                                        removeDesc: {
                                            removeAny: true,
                                        },
                                    },
                                ],
                            },
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".js", ".graphql"],
        alias: {
            "@app": path.resolve(__dirname, "./src/app"),
            "@server": path.resolve(__dirname, "./src/server"),
            "@services": path.resolve(__dirname, "./src/services"),
        },
    },
    performance: {
        hints: false,
    },
    optimization: {
        minimizer: [
            new webpackTerser({
                terserOptions: {
                    output: {
                        comments: false,
                    },
                },
                extractComments: false,
            }),
            new webpack.BannerPlugin(banner),
        ],
    },
    plugins: [
        new webpack.ExtendedAPIPlugin(),
        new webpack.ProvidePlugin({
            Promise: "es6-promise-promise",
        }),
        new webpack.DefinePlugin({
            PACKAGE_NAME: JSON.stringify(package.name),
            PACKAGE_TITLE: JSON.stringify(package.title),
            PACKAGE_VERSION: JSON.stringify(package.version),
        }),
        new webpackCopy({
            patterns: [
                {
                    from: "src/server/views/",
                    to: "../../views/",
                    globOptions: {
                        ignore: ["**/pages/runner/autoscroll.ejs", "**/pages/runner/chat.ejs", "**/pages/runner/classic.ejs"],
                    },
                },
                { from: "node_modules/tripetto/fonts/", to: "../fonts/" },
                { from: "node_modules/tripetto/locales/", to: "../../locales/" },
            ],
        }),
        new webpackReplaceHash([
            {
                dir: "views/pages",
                files: ["app.ejs"],
                rules: [
                    {
                        search: /\[hash\]/,
                        replace: "[hash]",
                    },
                ],
            },
        ]),
        ...(!production
            ? [
                  new webpackLiveReload({
                      appendScriptTag: true,
                  }),
              ]
            : [
                  new analyzer({
                      analyzerMode: "static",
                      reportFilename: `../../reports/bundle-app.html`,
                      openAnalyzer: false,
                  }),
              ]),
    ],
};

const runners = ["autoscroll", "chat", "classic"].map((runner) => ({
    entry: `./src/runners/${runner}.ts`,
    output: {
        filename: `bundle-[hash].js`,
        path: path.resolve(__dirname, "static", "js", runner),
        library: "Tripetto",
        libraryTarget: "umd",
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig-runners.json",
                    compilerOptions: {
                        noEmit: false,
                        target: "ES5",
                        module: "commonjs",
                    },
                },
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".js"],
        alias: {
            "@services": path.resolve(__dirname, "./src/services"),
            "@server": path.resolve(__dirname, "./src/server"),
        },
    },
    performance: {
        hints: false,
    },
    optimization: {
        minimizer: [
            new webpackTerser({
                terserOptions: {
                    output: {
                        comments: false,
                    },
                },
                extractComments: false,
            }),
            new webpack.BannerPlugin(banner),
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            PACKAGE_NAME: JSON.stringify(package.name),
            PACKAGE_TITLE: JSON.stringify(package.title),
            PACKAGE_VERSION: JSON.stringify(package.version),
        }),
        new webpackCopy({
            patterns: [
                { from: `src/server/views/pages/runner/${runner}.ejs`, to: `../../../views/pages/runner/${runner}.ejs` },
                { from: `node_modules/tripetto-runner-${runner}/runner/index.js`, to: "runner.js" },
                { from: `node_modules/tripetto-runner-${runner}/builder/index.js`, to: "builder.js" },
            ],
        }),
        new webpackReplaceHash([
            {
                dir: "views/pages/runner",
                files: [`${runner}.ejs`],
                rules: [
                    {
                        search: /\[hash\]/,
                        replace: "[hash]",
                    },
                ],
            },
        ]),
        ...(production
            ? [
                  new analyzer({
                      analyzerMode: "static",
                      reportFilename: `../../../reports/bundle-runner-${runner}.html`,
                      openAnalyzer: false,
                  }),
              ]
            : []),
    ],
}));

const services = (target) => {
    return {
        entry: "./src/services/index.ts",
        output: {
            filename: "index.js",
            path: path.resolve(__dirname, "services", "dist", target),
            library: "TripettoServices",
            libraryTarget: target === "umd" ? "umd" : "commonjs2",
            libraryExport: (target === "umd" && "default") || "",
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    loader: "ts-loader",
                    options: {
                        configFile: "tsconfig-services.json",
                        compilerOptions: {
                            noEmit: false,
                            target: target === "es6" ? "ES6" : "ES5",
                            module: target === "es6" ? "es6" : "commonjs",
                        },
                    },
                },
            ],
        },
        resolve: {
            extensions: [".ts", ".js"],
            alias: {
                "@server": path.resolve(__dirname, "./src/server"),
            },
        },
        externals: {
            tripetto: target === "umd" ? "Tripetto" : "commonjs tripetto",
            "tripetto-runner-foundation": target === "umd" ? "TripettoRunner" : "commonjs tripetto-runner-foundation",
        },
        performance: {
            hints: false,
        },
        optimization: {
            minimizer: [
                new webpackTerser({
                    terserOptions: {
                        output: {
                            comments: false,
                        },
                    },
                    extractComments: false,
                }),
                new webpack.BannerPlugin(require("./tasks/services/banner.js")),
            ],
        },
        plugins: [
            new webpack.ProvidePlugin({
                Promise: "es6-promise-promise",
            }),
            new webpack.DefinePlugin({
                PACKAGE_NAME: JSON.stringify(require("./src/services/package.json").name),
                PACKAGE_TITLE: JSON.stringify(require("./src/services/package.json").title),
                PACKAGE_VERSION: JSON.stringify(package.version),
            }),
            new webpackCopy({
                patterns: [
                    { from: "src/services/package.json", to: "../../" },
                    { from: "src/services/.npmignore", to: "../../" },
                    { from: "src/services/README.md", to: "../../" },
                ],
            }),
            ...(production && target === "umd"
                ? [
                      new analyzer({
                          analyzerMode: "static",
                          reportFilename: `../../../reports/bundle-services.html`,
                          openAnalyzer: false,
                      }),
                  ]
                : []),
        ],
    };
};

module.exports = (env, argv) => {
    return [app, ...runners, ...(production ? [services("umd"), services("es5"), services("es6")] : [])];
};
