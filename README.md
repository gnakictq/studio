![Tripetto](https://unpkg.com/tripetto/assets/banner.svg)

Tripetto is a full-fledged form kit. Rapidly build and run smart flowing forms and surveys. Drop the kit in your codebase and use all of it or just the parts you need. The visual [**builder**](https://www.npmjs.com/package/tripetto) is for form building, and the [**runners**](https://www.npmjs.com/package/tripetto-runner-foundation) are for running those forms in different UI variants. It is entirely extendible and customizable. Anyone can build their own building [**blocks**](https://docs.tripetto.com/guide/blocks) (e.g., question types) or runner UI's.

# Tripetto Studio
[![Pipeline status](https://gitlab.com/tripetto/studio/badges/production/pipeline.svg)](https://gitlab.com/tripetto/studio/commits/production)
[![Styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Join the community on Spectrum](https://withspectrum.github.io/badge/badge.svg)](https://spectrum.chat/tripetto)
[![Follow us on Twitter](https://img.shields.io/twitter/follow/tripetto.svg?style=social&label=Follow)](https://twitter.com/tripetto)

Use the [Tripetto Studio](https://tripetto.app) to create and deploy powerful online interactions with your audience. Build cleverly flowing forms and surveys on a self-organizing 2D drawing board and share these with a link or embedded in a website or application. Or use Tripetto for easy composing only and then bypass it completely by self-hosting what you build and run.

# Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/studio/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
