const package = require("../../package.json");
const services = require("../../services/package.json");
const fs = require("fs");
const prettier = require("prettier");

services.version = package.version;
services.repository = package.repository;
services.author = package.author;
services.license = package.license;
services.homepage = package.homepage;
services.private = package.version;
services.private = false;
services.scripts = {};

console.log(`Services package version: ${services.version}`);

fs.writeFileSync(
    "./services/package.json",
    prettier.format(JSON.stringify(services), {
        parser: "json",
    }),
    "utf8"
);
