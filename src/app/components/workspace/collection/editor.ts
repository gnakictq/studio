import { Components, Forms, Layers, MoveableLayout, assert, pgettext } from "tripetto";
import { IStudioStyle } from "@app/components/studio/style";

export class CollectionEditor extends Components.Controller<Components.Collection> {
    static open(layer: Layers.Layer, collection: Components.Collection): CollectionEditor {
        return new CollectionEditor(layer, collection);
    }

    static openPanel(parent: Layers.Layer, collection: Components.Collection): Layers.Layer {
        const pStyle =
            collection.layout && (collection.layout as MoveableLayout<Components.Workspace, Components.Collection, IStudioStyle>).style;

        return assert(
            parent.createPanel(
                (layer: Layers.Layer) => {
                    this.open(layer, collection);
                },
                Layers.Layer.configuration
                    .width(pStyle ? pStyle.forms.width.small : 0)
                    .animation(Layers.LayerAnimations.Zoom)
                    .autoCloseChildPanels("stroke")
            )
        );
    }

    private constructor(layer: Layers.Layer, collection: Components.Collection) {
        super(
            layer,
            collection,
            pgettext("studio", "Collection properties"),
            "compact",
            collection.layout && (collection.layout as MoveableLayout<Components.Workspace, Components.Collection, IStudioStyle>).style
        );
    }

    private get collection(): Components.Collection {
        return this.ref;
    }

    onCards(cards: Components.Cards): void {
        cards.add(
            new Forms.Form({
                title: pgettext("studio", "Name"),
                controls: [
                    new Forms.Text("singleline", Forms.Text.bind(this.collection, "name", ""))
                        .escape(() => {
                            this.close();

                            return true;
                        })
                        .enter(() => {
                            this.close();

                            return true;
                        })
                        .placeholder(pgettext("studio", "Unnamed collection"))
                        .autoFocus()
                        .autoSelect(),
                ],
            })
        );
    }
}
