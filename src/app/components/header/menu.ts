import { Components, DOM, linearicon, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import * as URLS from "@app/urls";
import ASSET_NPM from "../../assets/logos/npm.svg";

export class Menu<T> extends Components.ToolbarMenu<T> {
    private icon: number;

    constructor(studio: StudioComponent, icon: number, fnMenu: () => Components.MenuOption[]) {
        super(studio.style.header.user, undefined, fnMenu);

        this.icon = icon;
    }

    onDraw(toolbar: Components.Toolbar<T>, element: DOM.Element): void {
        super.onDraw(toolbar, element);

        element.create("i", (icon: DOM.Element) => linearicon(this.icon, icon));
    }
}

export function supportMenu(studio: StudioComponent): Components.MenuOption[] {
    return [
        new Components.MenuLinkWithIcon(0xe933, pgettext("studio", "Help center"), URLS.HELP),
        new Components.MenuLinkWithIcon(0xe627, pgettext("studio", "Examples"), URLS.EXAMPLES),
        new Components.MenuLinkWithIcon(0xe6f9, pgettext("studio", "Tutorials"), URLS.TUTORIALS),
        new Components.MenuItemWithIcon(0xe712, pgettext("studio", "Cheatsheet"), () => studio.openCheatsheet()),
        new Components.MenuLinkWithIcon(0xe90a, pgettext("studio", "Report issue/bug"), URLS.ISSUES),
        new Components.MenuLinkWithIcon(0xe695, pgettext("studio", "Receive updates"), URLS.SUBSCRIBE),
    ];
}

export function contactMenu(): Components.MenuOption[] {
    return [
        new Components.MenuLinkWithIcon(0xe8ae, pgettext("studio", "Follow us"), URLS.TWITTER),
        new Components.MenuLinkWithIcon(0xe777, pgettext("studio", "Contact us"), URLS.CONTACT),
        new Components.MenuLinkWithIcon(0xe668, pgettext("studio", "Terms of use"), URLS.TERMS),
    ];
}

export function developersMenu(): Components.MenuOption[] {
    return [
        new Components.MenuLinkWithIcon(0xe90c, pgettext("studio", "Browse source"), URLS.SOURCE),
        new Components.MenuLinkWithIcon(0xe6d6, pgettext("studio", "Read documentation"), URLS.DOCUMENTATION),
        new Components.MenuLinkWithIcon(0xe7d7, pgettext("studio", "Discuss"), URLS.DISCUSS),
        new Components.MenuSeparator(),
        new Components.MenuSubmenuWithImage(`${ASSET_NPM}`, pgettext("studio", "Packages on npm"), [
            new Components.MenuLabel(pgettext("studio", "Form kit")),
            new Components.MenuLink(pgettext("studio", "Tripetto Builder"), URLS.NPM.BUILDER),
            new Components.MenuLink(pgettext("studio", "Tripetto Runner Foundation"), URLS.NPM.RUNNER),
            new Components.MenuLabel(pgettext("studio", "Studio")),
            new Components.MenuLink(pgettext("studio", "Tripetto Services"), URLS.NPM.SERVICES),
            new Components.MenuLabel(pgettext("studio", "Runners")),
            new Components.MenuLink(pgettext("studio", "Tripetto Autoscroll Runner"), URLS.NPM.RUNNERS.AUTOSCROLL),
            new Components.MenuLink(pgettext("studio", "Tripetto Chat Runner"), URLS.NPM.RUNNERS.CHAT),
            new Components.MenuLink(pgettext("studio", "Tripetto Classic Runner"), URLS.NPM.RUNNERS.CLASSIC),
        ]),
    ];
}
