import { Components, Debounce, Forms, Layers, REGEX_IS_URL, pgettext } from "tripetto";
import { StudioComponent } from "@app/components/studio";
import superagent from "superagent";
import { REGEX_IS_EMAIL } from "../../helpers/regex";
import { TEST_SLACK, TEST_WEBHOOK } from "@server/endpoints";
import { HELP_NOTIFICATION, HELP_SLACK, HELP_WEBHOOK } from "@app/urls";
import { IHookSettings } from "@server/entities/definitions/interface";
import { IUser } from "@server/entities/users";
import * as UpdateHookSettingsQuery from "@app/queries/definitions/update-hooks.graphql";
import { mutate } from "@app/helpers/api";

export class AutomateComponent extends Components.Controller<{
    readonly definitionId?: string;
    readonly hooks: IHookSettings;
    readonly user?: IUser;
}> {
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(studio: StudioComponent, hooks: IHookSettings, definitionId?: string): AutomateComponent {
        return studio.openPanel(
            (panel: Layers.Layer) => new AutomateComponent(panel, studio, hooks, definitionId),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, studio: StudioComponent, hooks: IHookSettings, definitionId?: string) {
        super(
            layer,
            {
                definitionId,
                hooks,
                user: studio.userAccount,
            },
            pgettext("studio", "Automate"),
            "compact",
            studio.style,
            "right",
            "on-when-validated",
            pgettext("studio", "Close")
        );

        layer.hook("OnReady", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private testSlack(url: string, includeFields: boolean): Promise<boolean> {
        return new Promise((fnResolve: (succeeded: boolean) => void) => {
            superagent
                .post(TEST_SLACK)
                .send({ url, includeFields })
                .then((res) => {
                    fnResolve(res.status === 200);
                })
                .catch(() => {
                    fnResolve(false);
                });
        });
    }

    private testWebhook(url: string, nvp: boolean): Promise<boolean> {
        return new Promise((fnResolve: (succeeded: boolean) => void) => {
            superagent
                .post(TEST_WEBHOOK)
                .send({ url, nvp })
                .then((res) => {
                    fnResolve(res.status === 200);
                })
                .catch(() => {
                    fnResolve(false);
                });
        });
    }

    onCards(cards: Components.Cards): void {
        const updateSettings = new Debounce(() => {
            if (this.ref.definitionId) {
                mutate({
                    query: UpdateHookSettingsQuery,
                    variables: {
                        id: this.ref.definitionId,
                        hooks: { email: emailSettings, slack: slackSettings, webhook: webhookSettings },
                    },
                });
            }
        }, 100);

        if (!this.ref.definitionId) {
            cards.add(
                new Forms.Form({
                    controls: [
                        new Forms.Notification(
                            pgettext(
                                "studio",
                                "An account is required to use automation. Sign in or create an account with just your email address to do so."
                            ),
                            "info"
                        ),
                    ],
                })
            );
        }

        const emailSettings =
            this.ref.hooks.email || (this.ref.hooks.email = { enabled: false, recipient: this.ref.user && this.ref.user.emailAddress });
        const emailSettingsGroup = new Forms.Group([]);

        const emailIncludeFields = new Forms.Checkbox(
            pgettext("studio", "Include response data in the message"),
            emailSettings.includeFields
        ).on((checkbox: Forms.Checkbox) => {
            emailSettings.includeFields = checkbox.isChecked;
            updateSettings.invoke();
        });
        const emailRecipientLabel = new Forms.Static(pgettext("studio", "Send a notification to:"));
        const emailRecipient = new Forms.Email((emailSettings && emailSettings.recipient) || "")
            .autoValidate((email: Forms.Text) =>
                email.value === "" || email.isDisabled || !email.isObservable
                    ? "unknown"
                    : REGEX_IS_EMAIL.test(email.value)
                    ? "pass"
                    : "fail"
            )
            .on((email: Forms.Email) => {
                if (emailSettings && !email.isInvalid) {
                    emailSettings.recipient = email.value;

                    updateSettings.invoke();
                }
            });
        const optionEmail = new Forms.Checkbox(
            pgettext("studio", "Send an email when someone completes your form ([learn more](%1))", HELP_NOTIFICATION),
            emailSettings.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;
                emailSettings.enabled = checked;
                updateSettings.invoke();

                emailSettingsGroup.visible(checked);
            });

        emailSettingsGroup.controls.push(emailRecipientLabel, emailRecipient, emailIncludeFields);
        emailSettingsGroup.visible(optionEmail.isChecked);

        cards.add(
            new Forms.Form({
                title: pgettext("studio", "📧 Email notification"),
                controls: [optionEmail, emailSettingsGroup],
            })
        ).isDisabled = this.ref.definitionId ? false : true;

        const slackSettings = this.ref.hooks.slack || (this.ref.hooks.slack = { enabled: false });
        const slackSettingsGroup = new Forms.Group([]);

        const slackIncludeFields = new Forms.Checkbox(
            pgettext("studio", "Include response data in the message"),
            slackSettings.includeFields
        ).on((checkbox: Forms.Checkbox) => {
            slackSettings.includeFields = checkbox.isChecked;
            updateSettings.invoke();
        });
        const slackUrl = new Forms.Text("singleline", slackSettings.url)
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((url: Forms.Text) =>
                url.value === "" || url.isDisabled || !url.isObservable ? "unknown" : REGEX_IS_URL.test(url.value) ? "pass" : "fail"
            )
            .on((text: Forms.Text) => {
                slackSettings.url = text.value;
                updateSettings.invoke();

                slackTestButton.buttonType = "normal";
                slackTestButton.disabled(!slackUrl.value || !REGEX_IS_URL.test(slackUrl.value));
                slackTestButton.label(pgettext("studio", "🩺 Test"));
            });
        const slackTestButton = new Forms.Button(pgettext("studio", "🩺 Test")).on((testButton) => {
            slackUrl.disable();
            testButton.disable();
            testButton.buttonType = "normal";
            testButton.label(pgettext("studio", "⏳ Testing..."));

            this.testSlack(slackUrl.value, slackIncludeFields.isChecked).then((succeeded) => {
                slackUrl.enable();
                testButton.enable();
                testButton.buttonType = succeeded ? "accept" : "warning";
                testButton.label(succeeded ? pgettext("studio", "✔ All good!") : pgettext("studio", "❌ Something's wrong"));
            });
        });

        const optionSlack = new Forms.Checkbox(
            pgettext("studio", "Send a Slack message when someone completes your form ([learn more](%1))", HELP_SLACK),
            slackSettings.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;

                slackSettings.enabled = checked;
                updateSettings.invoke();

                slackSettingsGroup.visible(checked);
            });

        slackSettingsGroup.controls.push(
            new Forms.Static(
                pgettext(
                    "studio",
                    "Please create an incoming [Slack webhook](https://api.slack.com/incoming-webhooks) and enter the URL of the webhook here:"
                )
            ).markdown(),
            slackUrl,
            slackIncludeFields,
            slackTestButton
        );
        slackSettingsGroup.visible(optionSlack.isChecked);

        cards.add(
            new Forms.Form({
                title: pgettext("studio", "📣 Slack notification"),
                controls: [optionSlack, slackSettingsGroup],
            })
        ).isDisabled = this.ref.definitionId ? false : true;

        const webhookSettings = this.ref.hooks.webhook || (this.ref.hooks.webhook = { enabled: false, nvp: true });
        const webhookSettingsGroup = new Forms.Group([]);
        const webhookRaw = new Forms.Checkbox(pgettext("studio", "Send raw response data to webhook"), !webhookSettings.nvp)
            .description(
                pgettext(
                    "studio",
                    "This option sends way more data to your webhook. Unfortunately most webhooks (like Zapier, Integromat, etc.) don't like that and want plain name-value pairs. In that case you should keep this option disabled."
                )
            )
            .on((checkbox: Forms.Checkbox) => {
                webhookSettings.nvp = !checkbox.isChecked;
                updateSettings.invoke();
            });

        const webhookTestButton = new Forms.Button(pgettext("studio", "🩺 Test"));
        const webhookUrlLabel = new Forms.Static(pgettext("studio", "Post the data to the following URL:"));
        const webhookUrl = new Forms.Text("singleline", webhookSettings.url)
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((url: Forms.Text) =>
                url.value === "" || url.isDisabled || !url.isObservable ? "unknown" : REGEX_IS_URL.test(url.value) ? "pass" : "fail"
            )
            .on((text: Forms.Text) => {
                webhookSettings.url = text.value;
                updateSettings.invoke();

                webhookTestButton.buttonType = "normal";
                webhookTestButton.disabled(!webhookUrl.value || !REGEX_IS_URL.test(webhookUrl.value));
                webhookTestButton.label(pgettext("studio", "🩺 Test"));
            });
        webhookTestButton.on((testButton) => {
            webhookUrl.disable();
            testButton.disable();
            testButton.buttonType = "normal";
            testButton.label(pgettext("studio", "⏳ Testing..."));

            this.testWebhook(webhookUrl.value, !webhookRaw.isChecked).then((succeeded) => {
                webhookUrl.enable();
                testButton.enable();
                testButton.buttonType = succeeded ? "accept" : "warning";
                testButton.label(succeeded ? pgettext("studio", "✔ All good!") : pgettext("studio", "❌ Something's wrong"));
            });
        });

        const optionWebhook = new Forms.Checkbox(
            pgettext("studio", "Notify an external service when someone completes your form ([learn more](%1))", HELP_WEBHOOK),
            webhookSettings.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;

                webhookSettings.enabled = checked;
                updateSettings.invoke();

                webhookSettingsGroup.visible(checked);
            });

        webhookSettingsGroup.controls.push(webhookUrlLabel, webhookUrl, webhookRaw, webhookTestButton);
        webhookSettingsGroup.visible(optionWebhook.isChecked);

        cards.add(
            new Forms.Form({
                title: pgettext("studio", "🔗 Webhook"),
                controls: [optionWebhook, webhookSettingsGroup],
            })
        ).isDisabled = this.ref.definitionId ? false : true;
    }
}
