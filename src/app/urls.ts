import { URL } from "./globals";

export const HELP = "https://tripetto.com/help/studio/";
export const HELP_RUNNERS = "https://tripetto.com/help/articles/how-to-switch-between-form-faces/";
export const HELP_STYLES = "https://tripetto.com/help/articles/how-to-style-your-forms/";
export const HELP_L10N = "https://tripetto.com/help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/";
export const HELP_PREVIEW = "https://tripetto.com/help/articles/how-to-let-the-live-preview-work-for-you/";
export const HELP_SHARE = "https://tripetto.com/help/articles/how-to-run-your-form-from-the-studio/";
export const HELP_SHARE_LINK = "https://tripetto.com/help/articles/how-to-share-a-link-to-your-form-from-the-studio/";
export const HELP_SHARE_EMBED = "https://tripetto.com/help/articles/how-to-embed-a-form-from-the-studio-into-your-website/";
export const HELP_RESULTS = "https://tripetto.com/help/articles/how-to-get-your-results-from-the-studio/";
export const HELP_EXPORT = "https://tripetto.com/help/articles/troubleshooting-seeing-multiple-download-buttons/";
export const HELP_NOTIFICATION = "https://tripetto.com/help/articles/how-to-automate-email-notifications-for-each-new-result/";
export const HELP_SLACK = "https://tripetto.com/help/articles/how-to-automate-slack-notifications-for-each-new-result/";
export const HELP_WEBHOOK = "https://tripetto.com/help/articles/how-to-automate-a-webhook-for-each-new-result-using-zapier/";

export const EXAMPLES = "https://tripetto.com/examples/";
export const TUTORIALS = "https://tripetto.com/tutorials/";
export const ISSUES = "https://gitlab.com/tripetto/studio/issues";
export const SUBSCRIBE = "https://tripetto.com/subscribe/";
export const TWITTER = "https://twitter.com/tripetto";
export const CONTACT = "https://tripetto.com/contact/";
export const TERMS = `${URL}/terms`;
export const SOURCE = "https://gitlab.com/tripetto/studio";
export const DOCUMENTATION = "https://docs.tripetto.com";
export const DISCUSS = "https://spectrum.chat/tripetto";
export const NPM = {
    BUILDER: "https://www.npmjs.com/package/tripetto",
    SERVICES: "https://www.npmjs.com/package/tripetto-services",
    RUNNER: "https://www.npmjs.com/package/tripetto-runner-foundation",
    RUNNERS: {
        AUTOSCROLL: "https://www.npmjs.com/package/tripetto-runner-autoscroll",
        CHAT: "https://www.npmjs.com/package/tripetto-runner-chat",
        CLASSIC: "https://www.npmjs.com/package/tripetto-runner-classic",
    },
};
