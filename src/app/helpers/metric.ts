import * as Superagent from "superagent";
import { METRIC } from "@server/endpoints";
import { MetricEventType, MetricSubjectType } from "@server/providers/metric";

export function addMetric(event: MetricEventType, subject?: MetricSubjectType, subjectId?: string): void {
    Superagent.post(METRIC).send({ event, subject, subjectId }).end();
}
