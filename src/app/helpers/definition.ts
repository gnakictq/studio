import { IDefinition, assert } from "tripetto";
import { ICreateResult } from "@server/entities/mutationresult/interface";
import { mutate } from "./api";
import * as CreateQuery from "@app/queries/definitions/create.graphql";
import * as UpdateQuery from "@app/queries/definitions/update.graphql";
import * as UpdateStylesQuery from "@app/queries/definitions/update-styles.graphql";
import * as UpdateL10nQuery from "@app/queries/definitions/update-l10n.graphql";
import { TL10n, TStyles } from "tripetto-runner-foundation";
import { TRunners } from "@server/entities/definitions/runners";

export async function createDefinition(
    runner: TRunners,
    definition: IDefinition,
    styles: TStyles | undefined,
    l10n: TL10n | undefined,
    onError: (isAuthenticated: boolean) => void
): Promise<string | undefined> {
    const result = assert(
        await mutate<ICreateResult>({
            query: CreateQuery,
            variables: {
                runner,
            },
            onError,
        })
    );
    return mutate({
        query: UpdateQuery,
        variables: {
            data: {
                id: result.id,
                name: (definition && definition.name) || "",
                data: definition,
            },
        },
        onError,
    })
        .then(() => {
            styles
                ? mutate({
                      query: UpdateStylesQuery,
                      variables: {
                          id: result.id,
                          styles,
                      },
                      onError,
                  })
                : Promise.resolve();
        })
        .then(() => {
            l10n
                ? mutate({
                      query: UpdateL10nQuery,
                      variables: {
                          id: result.id,
                          l10n,
                      },
                      onError,
                  })
                : Promise.resolve();
        })
        .then(() => result.id);
}
