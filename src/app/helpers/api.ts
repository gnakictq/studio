import { DocumentNode } from "graphql";
import ApolloClient, { ApolloQueryResult } from "apollo-client";
import { ApolloLink, FetchResult } from "apollo-link";
import { createHttpLink } from "apollo-link-http";
import { ErrorResponse } from "apollo-link-error";
import { RetryLink } from "apollo-link-retry";
import { ServerError } from "apollo-link-http-common";
import { InMemoryCache } from "apollo-cache-inmemory";
import { API } from "@server/endpoints";
import fetch from "unfetch";

const link = ApolloLink.from([
    new RetryLink({
        delay: {
            initial: 100,
        },
        attempts: {
            max: 3,
        },
    }) as ApolloLink,
    createHttpLink({
        uri: API,
        credentials: "same-origin",
        fetch,
    }) as ApolloLink,
]);

const client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
});

function onApiError(onError?: (authenticated: boolean) => void): (err: ErrorResponse) => undefined {
    return (error: ErrorResponse) => {
        let authenticated = true;
        const networkError = error.networkError as ServerError;
        if (networkError && networkError.statusCode && networkError.statusCode === 401) {
            authenticated = false;
        }

        if (onError) {
            onError(authenticated);
        } else {
            // Let it go.
        }
        return undefined;
    };
}

interface IQueryProperties {
    query: DocumentNode;
    variables?: {};
    onError?: (authenticated: boolean) => void;
}

export async function query<T>(props: IQueryProperties): Promise<T | undefined> {
    return client
        .query({
            query: props.query,
            variables: props.variables,
            fetchPolicy: "no-cache",
        })
        .then((value: ApolloQueryResult<{ [queryName: string]: T }>) => value.data[Object.keys(value.data)[0]])
        .catch(onApiError(props.onError));
}

export async function mutate<T>(props: IQueryProperties): Promise<T | undefined> {
    return client
        .mutate({
            mutation: props.query,
            variables: props.variables,
        })
        .then((value: FetchResult<{ [mutationName: string]: T }>) => value.data && value.data[Object.keys(value.data)[0]])
        .catch(onApiError(props.onError)) as Promise<T | undefined>;
}
