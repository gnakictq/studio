import * as Superagent from "superagent";
import { HEADER_RUNNER_TOKEN } from "@server/headers";

const PATH_UNCONFIRMED = `/unconfirmed`;

/** Downloads a file from the filestore.
 * @param fileStoreUrl The url of the filestore.
 * @param token The token that represents the definition.
 * @param file The file.
 */
export const downloadFile = (fileStoreUrl: string, token: string, file: string) =>
    new Promise<Blob>((resolve: (data: Blob) => void, reject: () => void) => {
        Superagent.get(`${fileStoreUrl}${PATH_UNCONFIRMED}/${file}`)
            .set(HEADER_RUNNER_TOKEN, token)
            .responseType("blob")
            .then((res: Superagent.Response) => {
                if (res.status === 200) {
                    resolve(res.body);
                } else {
                    reject();
                }
            })
            .catch(() => reject());
    });

/** Uploads a file to the filestore.
 * @param fileStoreUrl The url of the file store.
 * @param token The token that represents the definition.
 * @param file The file.
 * @param onProgress Function for handling the progress of the file upload.
 */
export const uploadFile = (fileStoreUrl: string, token: string, file: File, onProgress: (percentage: number) => void) =>
    new Promise<string>((resolve: (id: string) => void, reject: (reason?: string) => void) => {
        const formData = new FormData();

        formData.append("file", file);

        Superagent.post(fileStoreUrl)
            .set(HEADER_RUNNER_TOKEN, token)
            .send(formData)
            .on("progress", (event: Superagent.ProgressEvent) => {
                if (event.direction === "upload") {
                    onProgress(event.percent || 0);
                }
            })
            .then((res: Superagent.Response) => {
                if (res.status === 200 && res.body?.name) {
                    return resolve(res.body.name);
                } else {
                    return reject(res.status === 413 ? "File is too large." : undefined);
                }
            })
            .catch(() => reject());
    });

/** Deletes a file from the filestore.
 * @param fileStoreUrl The url of the filestore.
 * @param token The token that represents the definition.
 * @param file The file.
 * @param onProgress Function for handling the progress of the file upload.
 */
export const deleteFile = (fileStoreUrl: string, token: string, file: string) =>
    new Promise<void>((resolve: () => void, reject: () => void) => {
        Superagent.delete(fileStoreUrl + PATH_UNCONFIRMED)
            .set(HEADER_RUNNER_TOKEN, token)
            .send({ file })
            .then((res: Superagent.Response) => {
                if (res.status === 200) {
                    return resolve();
                } else {
                    return reject();
                }
            })
            .catch(() => reject());
    });
