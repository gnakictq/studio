/** Specifies the url of Tripetto Studio server. */
export const URL = "https://tripetto.app";

/** Specifies the url of the Tripetto Filestore. */
export const FILESTORE_URL = "https://filestore.tripetto.app";
