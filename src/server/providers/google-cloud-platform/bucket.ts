import { Bucket, File, Storage } from "@google-cloud/storage";
import { ENV, GOOGLE_CLOUD_PROJECT, WRITE_FILE_RETRY_TRESHOLD } from "../../settings";
import { IFileProvider } from "../file";
import { inject, injectable } from "inversify";
import { logProviderSymbol } from "../../symbols";
import { ILogProvider } from "../log";

@injectable()
export class BucketProvider implements IFileProvider {
    /** Stack with file content to be written to Google Cloud Platform. */
    private readonly writeStack: {
        [filename: string]: { content: string; retryCount: number };
    } = {};
    private readonly logProvider: ILogProvider;
    protected readonly bucket: Bucket;

    constructor(@inject(logProviderSymbol) logProvider: ILogProvider) {
        this.bucket = new Storage({
            projectId: GOOGLE_CLOUD_PROJECT,
        }).bucket(`${GOOGLE_CLOUD_PROJECT}-${ENV}`);
        this.logProvider = logProvider;
    }

    private writeFile(filename: string, content: string): void {
        const stackEntry = this.writeStack[filename];
        const inRetryMode = stackEntry && stackEntry.retryCount > 0;

        if (!stackEntry || inRetryMode) {
            setTimeout(
                () => {
                    const executingStackEntry = this.writeStack[filename];
                    if (executingStackEntry) {
                        if (executingStackEntry.retryCount <= WRITE_FILE_RETRY_TRESHOLD) {
                            this.bucket
                                .file(filename)
                                .save(executingStackEntry.content)
                                .then(() => delete this.writeStack[filename])
                                .catch((err: {}) => {
                                    this.logProvider.error(err);
                                    this.writeStack[filename].retryCount += 1;
                                    this.writeFile(filename, executingStackEntry.content);
                                });
                        } else {
                            this.logProvider.critical({
                                message: `Failed to write file content to ${filename} in ${executingStackEntry.retryCount} attempts.`,
                                content: executingStackEntry.content,
                            });
                            this.writeStack[filename].retryCount = 1;
                        }
                    }
                },
                // Throttle write actions to max. 1 per filename per second.
                // See https://cloud.google.com/storage/quotas
                1500
            );
        }

        if (!inRetryMode) {
            this.writeStack[filename] = { content, retryCount: 0 };
        }
    }

    private async getExistingFile(filename: string): Promise<File | undefined> {
        const file = this.bucket.file(filename);
        return file.exists().then(([exists]: [boolean]) => (exists ? file : undefined));
    }

    private async readFile(filename: string): Promise<Buffer | string | undefined> {
        const contentFromStack = this.writeStack[filename];
        if (contentFromStack) {
            return contentFromStack.content;
        }

        return this.getExistingFile(filename).then((file?: File) => {
            if (!file) {
                return Promise.resolve(undefined);
            }
            return file.download().then(([buffer]: [Buffer]) => buffer);
        });
    }

    private async deleteFile(filename: string): Promise<void> {
        return this.getExistingFile(filename).then(async (file?: File) => {
            if (file) {
                return file.delete().then(() => Promise.resolve());
            }
        });
    }

    private async deleteFiles(prefix: string): Promise<void> {
        return this.bucket.deleteFiles({ prefix });
    }

    private userPath(userId: string): string {
        return `users/${userId}`;
    }

    private workspacePath(userId: string): string {
        return `${this.userPath(userId)}/workspaces`;
    }

    private workspaceFilename(userId: string, workspaceId: string): string {
        return `${this.workspacePath(userId)}/${workspaceId}.json`;
    }

    private definitionPath(userId: string, definitionId: string): string {
        return `${this.userPath(userId)}/definitions/${definitionId}`;
    }

    private definitionFilename(userId: string, definitionId: string): string {
        return `${this.definitionPath(userId, definitionId)}/definition.json`;
    }

    private responsePath(userId: string, definitionId: string): string {
        return `${this.definitionPath(userId, definitionId)}/responses`;
    }

    private responseFilename(userId: string, definitionId: string, responseId: string): string {
        return `${this.responsePath(userId, definitionId)}/${responseId}.json`;
    }

    private snapshotPath(userId: string, definitionId: string): string {
        return `${this.definitionPath(userId, definitionId)}/snapshots`;
    }

    private snapshotFilename(userId: string, definitionId: string, snapshotId: string): string {
        return `${this.snapshotPath(userId, definitionId)}/${snapshotId}.json`;
    }

    async deleteUser(userId: string): Promise<void> {
        return this.deleteFiles(this.userPath(userId));
    }

    async writeWorkspace(userId: string, workspaceId: string, content: string): Promise<void> {
        const filename = this.workspaceFilename(userId, workspaceId);
        return this.writeFile(filename, content);
    }

    async readWorkspace(userId: string, workspaceId: string): Promise<Buffer | string | undefined> {
        const filename = this.workspaceFilename(userId, workspaceId);
        return this.readFile(filename);
    }

    async deleteWorkspace(userId: string, workspaceId: string): Promise<void> {
        const filename = this.workspaceFilename(userId, workspaceId);
        return this.deleteFile(filename);
    }

    async writeDefinition(userId: string, definitionId: string, content: string): Promise<void> {
        const filename = this.definitionFilename(userId, definitionId);
        return this.writeFile(filename, content);
    }

    async readDefinition(userId: string, definitionId: string): Promise<Buffer | string | undefined> {
        const filename = this.definitionFilename(userId, definitionId);
        return this.readFile(filename);
    }

    async deleteDefinition(userId: string, definitionId: string): Promise<void> {
        const filename = this.definitionFilename(userId, definitionId);
        return this.deleteFile(filename);
    }

    async writeResponse(userId: string, definitionId: string, responseId: string, content: string): Promise<void> {
        const filename = this.responseFilename(userId, definitionId, responseId);
        return this.writeFile(filename, content);
    }

    async readResponse(userId: string, definitionId: string, responseId: string): Promise<Buffer | string | undefined> {
        const filename = this.responseFilename(userId, definitionId, responseId);
        return this.readFile(filename);
    }

    async deleteResponse(userId: string, definitionId: string, responseId: string): Promise<void> {
        const filename = this.responseFilename(userId, definitionId, responseId);
        return this.deleteFile(filename);
    }

    async deleteResponses(userId: string, definitionId: string): Promise<void> {
        return this.deleteFiles(this.responsePath(userId, definitionId));
    }

    async writeSnapshot(userId: string, definitionId: string, snapshotId: string, content: string): Promise<void> {
        const filename = this.snapshotFilename(userId, definitionId, snapshotId);
        return this.writeFile(filename, content);
    }

    async readSnapshot(userId: string, definitionId: string, snapshotId: string): Promise<Buffer | string | undefined> {
        const filename = this.snapshotFilename(userId, definitionId, snapshotId);
        return this.readFile(filename);
    }

    async deleteSnapshot(userId: string, definitionId: string, snapshotId: string): Promise<void> {
        const filename = this.snapshotFilename(userId, definitionId, snapshotId);
        return this.deleteFile(filename);
    }

    async deleteSnapshots(userId: string, definitionId: string): Promise<void> {
        return this.deleteFiles(this.snapshotPath(userId, definitionId));
    }
}
