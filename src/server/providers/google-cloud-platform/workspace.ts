import { FirestoreProvider } from "./firestore";
import { fileProviderSymbol } from "../../symbols";
import { inject, injectable } from "inversify";
import { IWorkspaceProvider } from "../workspace";
import { IWorkspace, IWorkspaceInput } from "../../entities/workspaces/interface";
import { DocumentReference, DocumentSnapshot, FieldValue, Timestamp } from "@google-cloud/firestore";
import { IFileProvider } from "../file";

const ROOT_ID = "root";

@injectable()
export class WorkspaceProvider extends FirestoreProvider implements IWorkspaceProvider {
    private readonly fileProvider: IFileProvider;

    constructor(@inject(fileProviderSymbol) fileProvider: IFileProvider) {
        super();
        this.fileProvider = fileProvider;
    }

    private async createRoot(userId: string): Promise<IWorkspace | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getWorkspacesPath(userId))
                .doc(ROOT_ID)
                .create(this.getServerTimestampValues())
                .then(() => {
                    const now = this.getNowInMilliseconds();
                    return {
                        id: ROOT_ID,
                        name: "",
                        tileCount: 0,
                        data: undefined,
                        created: now,
                        modified: now,
                    };
                });
        });
    }

    async create(userId: string): Promise<string | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .collection(this.getWorkspacesPath(userId))
                .add(this.getServerTimestampValues())
                .then((doc: DocumentReference) => doc.id);
        });
    }

    async read(userId: string, workspaceId: string, includeData: boolean): Promise<IWorkspace | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return this.firestore
                .doc(this.getWorkspacePath(userId, workspaceId))
                .get()
                .then((workspaceSnapshot: DocumentSnapshot) => {
                    if (!workspaceSnapshot.exists) {
                        return Promise.resolve(undefined);
                    }
                    const data = workspaceSnapshot.data();
                    if (!data) {
                        return undefined;
                    }

                    const workspace: IWorkspace = {
                        id: workspaceId,
                        name: data.name,
                        tileCount: data.tileCount || 0,
                        created: (data.created as Timestamp).toMillis(),
                        modified: (data.modified as Timestamp).toMillis(),
                    };

                    if (includeData) {
                        return this.fileProvider.readWorkspace(userId, workspaceId).then((file?: string | Buffer) => {
                            workspace.data = file && JSON.parse(file.toString());
                            return workspace;
                        });
                    }
                    return workspace;
                });
        });
    }

    async readRoot(userId: string, includeData: boolean): Promise<IWorkspace | undefined> {
        return this.read(userId, ROOT_ID, includeData).then((existingRoot?: IWorkspace) => {
            if (existingRoot) {
                return existingRoot;
            }

            return this.createRoot(userId);
        });
    }

    async update(userId: string, workspace: IWorkspaceInput): Promise<void> {
        return this.requireUserBeforeExecute(userId, async () => {
            return Promise.all([
                this.firestore.doc(this.getWorkspacePath(userId, workspace.id)).update({
                    name: workspace.name,
                    tileCount: workspace.tileCount,
                    modified: FieldValue.serverTimestamp(),
                }),
                this.fileProvider.writeWorkspace(userId, workspace.id, JSON.stringify(workspace.data)),
            ]).then(() => Promise.resolve());
        });
    }

    async delete(userId: string, workspaceId: string): Promise<void | undefined> {
        return this.requireUserBeforeExecute(userId, async () => {
            return Promise.all([
                this.fileProvider.deleteWorkspace(userId, workspaceId),
                // Only delete firestore document when file is actually deleted.
                this.firestore.doc(this.getWorkspacePath(userId, workspaceId)).delete(),
            ]).then(() => Promise.resolve());
        });
    }
}
