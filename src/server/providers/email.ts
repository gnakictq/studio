/** Describes a provider for sending emails. */
export interface IEmailServiceProvider {
    /** Sends an email. */
    readonly send: (options: IEmailOptions) => Promise<void>;
}

/** Describes the options for sending emails. */
export interface IEmailOptions {
    /** Emailaddress to send the email to. */
    to: string;

    /** Emailaddress from whom the email was sent. */
    from: { name?: string; email: string };

    /** Reaply-to address. */
    reply_to?: { name?: string; email: string };

    /** Subject of the email. */
    subject: string;

    /** Text of the email. */
    text?: string;

    /** Html of the email. */
    html: string;
}
