import { IResponse, IResponseAnnouncement, IResponseSubmission } from "../entities/responses/interface";
import { Export } from "tripetto-runner-foundation";

/** Describes the response provider. */
export interface IResponseProvider {
    /** Announces a response. */
    readonly announce: (
        userPublicKey: string,
        definitionPublicKey: string,
        fingerprint: string,
        checksum: string,
        runner: string,
        ip: string
    ) => Promise<IResponseAnnouncement | undefined>;

    /** Submit a response. */
    readonly submit: (
        userPublicKey: string,
        definitionPublicKey: string,
        announcementId: string,
        nonce: string,
        powHashRate: number,
        powDuration: number,
        language: string,
        locale: string,
        exportables: Export.IExportables,
        actionables: Export.IActionables | undefined
    ) => Promise<IResponseSubmission | undefined>;

    /** Read a response. */
    readonly read: (userId: string, definitionId: string, responseId: string, includeData: boolean) => Promise<IResponse | undefined>;

    /** Read all responses for a definition. */
    readonly readAllByDefinition: (userId: string, definitionId: string, includeData: boolean) => Promise<IResponse[] | undefined>;

    /** Read all responses for a definition and fingerprint. */
    readonly readAllByFingerprint: (userId: string, definitionId: string, fingerprint: string) => Promise<IResponse[] | undefined>;

    /** Read all responses for a definition and stencil. */
    readonly readAllByStencil: (userId: string, definitionId: string, stencil: string) => Promise<IResponse[] | undefined>;

    /** Read number of responses for a definition. */
    readonly readCountByDefinition: (userId: string, definitionId: string) => Promise<number | undefined>;

    /** List all responses for a definition. */
    readonly list: (userId: string, definitionId: string) => Promise<string[] | undefined>;

    /** List all attachments for a definition. */
    readonly listAttachments: (userId: string, definitionId: string) => Promise<string[] | undefined>;

    /** Delete a response. */
    readonly delete: (userId: string, definitionId: string, definitionPublicKey: string, responseId: string) => Promise<void | undefined>;

    /** Update the fingerprint of a response. */
    readonly updateFingerprint: (
        userId: string,
        definitionId: string,
        responseId: string,
        fingerprint: string
    ) => Promise<void | undefined>;
}
