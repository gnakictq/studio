import * as express from "express";

/** Gets the referer of the request. */
export function getReferer(request: express.Request): string | undefined {
    return request.get("referer");
}

/** Gets the session id of the request. */
export function getSessionId(request: express.Request): string | undefined {
    return request.session && request.session.id;
}
