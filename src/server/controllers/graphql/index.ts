import { WorkspaceResolver } from "../../entities/workspaces";
import { dependencyContainer } from "../../dependencies";
import { GraphQLError, GraphQLSchema } from "graphql";
import * as Express from "express";
import { IController } from "../controller";
import { authChecker } from "./auth-checker";
import { GraphQLParams, graphqlHTTP } from "express-graphql";
import { DefinitionResolver } from "../../entities/definitions";
import { UserResolver } from "../../entities/users";
import { buildSchema } from "type-graphql";
import { ResponseResolver } from "../../entities/responses";
import { BaseController } from "../base";
import { largeLogProviderSymbol, logProviderSymbol, tokenProviderSymbol } from "../../symbols";
import { ITokenProvider } from "../../providers/token";
import { inject, injectable } from "inversify";
import { API } from "../../endpoints";
import { ENV } from "../../settings";
import { ILogProvider } from "../../providers";

@injectable()
export class GraphqlController extends BaseController implements IController {
    private logProvider: ILogProvider;
    private largeLogProvider: ILogProvider;
    public router = Express.Router();

    constructor(
        @inject(tokenProviderSymbol) tokenProvider: ITokenProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider,
        @inject(largeLogProviderSymbol) largeLogProvider: ILogProvider
    ) {
        super(tokenProvider);
        this.logProvider = logProvider;
        this.largeLogProvider = largeLogProvider;

        this.initializeRoutes();
    }

    private async initializeRoutes(): Promise<void> {
        this.router.use(API, this.authenticateByCookie(true), this.verifyTokenType("auth", true), await this.api());
    }

    private async api(): Promise<(request: Express.Request, response: Express.Response) => Promise<void>> {
        return graphqlHTTP(async (req: Express.Request, res: Express.Response, params?: GraphQLParams) => {
            return {
                schema: await this.getGraphQLSchema(),
                graphiql: true,
                customFormatErrorFn: (error: GraphQLError) => {
                    this.largeLogProvider.debug(params || {});
                    this.logProvider.error({ message: error.message, error, requestHeaders: req.headers });

                    if (ENV !== "development") {
                        // Return as little information as possible. For more detail, check the log.
                        return { message: "Server error" };
                    }
                    return {
                        message: error.message,
                        locations: error.locations,
                        stack: error.stack ? error.stack.split("\n") : [],
                        path: error.path,
                    };
                },
            };
        });
    }

    private async getGraphQLSchema(): Promise<GraphQLSchema> {
        return buildSchema({
            container: dependencyContainer,
            resolvers: [UserResolver, WorkspaceResolver, DefinitionResolver, ResponseResolver],
            authChecker,
            authMode: "null",
            validate: false,
        });
    }
}
