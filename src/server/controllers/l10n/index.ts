import { IController } from "../controller";
import { injectable } from "inversify";
import { LOCALE, TRANSLATION } from "../../endpoints";
import { localePath } from "../../helpers/locales";
import { translationPath } from "../../helpers/translations";
import cors from "cors";
import * as Express from "express";

@injectable()
export class L10nController implements IController {
    public router = Express.Router();

    constructor() {
        this.router.get(LOCALE, cors({ methods: "GET" }), this.locale);
        this.router.get(TRANSLATION, cors({ methods: "GET" }), this.translation);
    }

    private get translation(): Express.RequestHandler {
        return (request: Express.Request, response: Express.Response) => {
            const language = (typeof request.query.l === "string" && request.query.l) || request.header("Accept-Language");
            const context = (typeof request.query.c === "string" && request.query.c) || "";
            const translation = translationPath(language, request.query.l ? false : true, context);

            response.setHeader("Content-Type", "application/json");

            if (translation) {
                response.sendFile(translation);
            } else {
                response.json(undefined);
            }
        };
    }

    private get locale(): Express.RequestHandler {
        return (request: Express.Request, response: Express.Response) => {
            const language = (typeof request.query.l === "string" && request.query.l) || request.header("Accept-Language");
            const locale = localePath(language);

            if (locale) {
                response.setHeader("Content-Type", "application/json");
                response.sendFile(locale);
            } else {
                response.sendStatus(406);
            }
        };
    }
}
