import {
    definitionProviderSymbol,
    emailServiceProviderSymbol,
    logProviderSymbol,
    metricProviderSymbol,
    responseProviderSymbol,
    snapshotProviderSymbol,
    tokenAliasProviderSymbol,
    tokenProviderSymbol,
} from "../../symbols";
import { IDefinition } from "../../entities/definitions";
import { BaseController } from "../base";
import superagent from "superagent";
import ejs from "ejs";
import path from "path";
import cors from "cors";
import express from "express";
import jwt from "jsonwebtoken";
import validator from "validator";
import { inject, injectable } from "inversify";
import {
    IDefinitionProvider,
    IEmailServiceProvider,
    ILogProvider,
    IMetricProvider,
    IResponseProvider,
    ISnapshotProvider,
    ITokenAliasProvider,
    ITokenProvider,
} from "../../providers";
import { IController, ITemplateTokenRequest } from "../controller";
import { HEADER_RUNNER_TOKEN, HEADER_TEMPLATE_TOKEN } from "../../headers";
import {
    EMAIL_FROM_ADDRESS,
    EMAIL_FROM_NAME,
    SENTRY_DSN_RUNNER,
    SHORT_ID_SIZE,
    TRIPETTO_APP_PK,
    TRIPETTO_FILESTORE_URL,
    URL,
} from "../../settings";
import {
    DEFINITION,
    L10N,
    RESPONSE_ANNOUNCE,
    RESPONSE_SUBMIT,
    RUN,
    SNAPSHOT,
    STYLES,
    TEMPLATE,
    TEST_SLACK,
    TEST_WEBHOOK,
} from "../../endpoints";
import { ISnapshot } from "../../entities/snapshots";
import { getReferer } from "../../helpers/request";
import { logCriticalError } from "../../helpers/error";
import { createHash } from "../../helpers/crypto";
import { IRunnerToken } from "../../entities/tokens/interface";
import { IEmailHookSettings, ISlackHookSettings, IWebHookSettings } from "../../entities/definitions/interface";
import { Export, Str, castToFloat, castToNumber, castToString, isString } from "tripetto-runner-foundation";
import { attachmentUrl, isAttachment } from "../../helpers/attachment";
import { formatDate, isDate } from "../../helpers/date";

@injectable()
export class RunnerController extends BaseController implements IController {
    private definitionProvider: IDefinitionProvider;
    private snapshotProvider: ISnapshotProvider;
    private emailServiceProvider: IEmailServiceProvider;
    private logProvider: ILogProvider;
    private metricProvider: IMetricProvider;
    private responseProvider: IResponseProvider;
    private tokenAliasProvider: ITokenAliasProvider;
    public router = express.Router();

    constructor(
        @inject(responseProviderSymbol) responseProvider: IResponseProvider,
        @inject(definitionProviderSymbol) definitionProvider: IDefinitionProvider,
        @inject(snapshotProviderSymbol) snapshotProvider: ISnapshotProvider,
        @inject(emailServiceProviderSymbol) emailServiceProvider: IEmailServiceProvider,
        @inject(tokenProviderSymbol) tokenProvider: ITokenProvider,
        @inject(tokenAliasProviderSymbol) tokenAliasProvider: ITokenAliasProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider,
        @inject(metricProviderSymbol) metricProvider: IMetricProvider
    ) {
        super(tokenProvider);

        this.responseProvider = responseProvider;
        this.definitionProvider = definitionProvider;
        this.snapshotProvider = snapshotProvider;
        this.emailServiceProvider = emailServiceProvider;
        this.logProvider = logProvider;
        this.metricProvider = metricProvider;
        this.tokenAliasProvider = tokenAliasProvider;

        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        this.router.get(RUN, this.runnerByReadTokenInQuerystring);

        this.router.options([DEFINITION, STYLES, L10N, SNAPSHOT], cors({ methods: "GET" }));
        this.router.get(DEFINITION, cors({ methods: "GET" }), this.definition);
        this.router.get(STYLES, cors({ methods: "GET" }), this.styles);
        this.router.get(L10N, cors({ methods: "GET" }), this.l10n);
        this.router.get(SNAPSHOT, cors({ methods: "GET" }), this.snapshot);

        this.router.options([RESPONSE_ANNOUNCE, RESPONSE_SUBMIT, SNAPSHOT], cors({ methods: "POST" }));
        this.router.post(RESPONSE_ANNOUNCE, cors({ methods: "POST" }), this.announceResponse);
        this.router.post(RESPONSE_SUBMIT, cors({ methods: "POST" }), this.submitResponse);
        this.router.post(SNAPSHOT, cors({ methods: "POST" }), this.postSnapshot);

        this.router.get(RUN + "/:alias", this.runnerByTokenAlias);

        this.router.get(
            TEMPLATE,
            this.verifyTokenInHeader(HEADER_TEMPLATE_TOKEN, true),
            this.verifyTokenType("template", true),
            this.template
        );

        this.router.post(TEST_SLACK, this.authenticateByCookie(true), this.testSlack);
        this.router.post(TEST_WEBHOOK, this.authenticateByCookie(true), this.testWebhook);

        // Legacy routes
        this.router.options(["/collect/definition", "/collect/styles", "/collect/l10n", "/collect/snapshot"], cors({ methods: "GET" }));
        this.router.options(["/collect/response", "/collect/snapshot"], cors({ methods: "POST" }));

        this.router.get("/collect", this.runnerByReadTokenInQuerystring);
        this.router.get("/collect/definition", cors({ methods: "GET" }), this.definition);
        this.router.get("/collect/styles", cors({ methods: "GET" }), this.styles);
        this.router.get("/collect/l10n", cors({ methods: "GET" }), this.l10n);
        this.router.get("/collect/snapshot", cors({ methods: "GET" }), this.snapshot);
        this.router.post("/collect/snapshot", cors({ methods: "POST" }), this.postSnapshot);
        this.router.post("/collect/response", cors({ methods: "POST" }), this.submitResponse);
        this.router.get("/collect/:alias", this.runnerByTokenAlias);
    }

    private runnerByReadToken(token: string, req: express.Request, res: express.Response, next: express.NextFunction): void {
        jwt.verify(token, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, runnerToken: IRunnerToken | undefined) => {
            if (!errors && runnerToken) {
                const getDefinition = () => {
                    this.definitionProvider
                        .readByPublicKey(runnerToken.user, runnerToken.definition, true)
                        .then(async (definition?: IDefinition) => {
                            if (!definition) {
                                res.status(404).render("pages/runner/not-found");
                                return;
                            }

                            let keywords = "";

                            if (definition.data && definition.data.keywords) {
                                definition.data.keywords.forEach((keyword: string) => {
                                    keywords += (keywords === "" ? "" : ",") + keyword;
                                });
                            }

                            res.render(`pages/runner/${definition.runner}`, {
                                cspNonce: res.locals.cspNonce,
                                token,
                                name: definition.name,
                                description: (definition.data && definition.data.description) || "",
                                language: (definition.data && definition.data.language) || "",
                                keywords,
                                url: URL + req.url,
                                filestoreUrl: TRIPETTO_FILESTORE_URL,
                                noBranding: definition.styles?.noBranding ? true : false,
                                sentryDSN: SENTRY_DSN_RUNNER,
                            });
                        })
                        .catch(next);
                };

                if (runnerToken.snapshot) {
                    this.snapshotProvider
                        .read(runnerToken.user, runnerToken.definition, runnerToken.snapshot, true)
                        .then((snapshot?: ISnapshot) => {
                            if (snapshot && snapshot.data) {
                                getDefinition();
                            } else {
                                res.status(401).render("pages/runner/completed");
                            }
                        });
                } else {
                    getDefinition();
                }
            } else {
                res.status(401).render("pages/runner/invalid");
            }
        });
    }

    private get runnerByReadTokenInQuerystring(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) =>
            this.runnerByReadToken((typeof req.query.token === "string" && req.query.token) || "", req, res, next);
    }

    private get runnerByTokenAlias(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) =>
            req.params.alias
                ? this.tokenAliasProvider.read(req.params.alias).then((token?: string) => {
                      if (!token) {
                          res.status(404).render("pages/runner/not-found");
                          return;
                      }

                      this.runnerByReadToken(token, req, res, next);
                  })
                : next();
    }

    private get definition(): express.RequestHandler {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const readTokenOrAlias = req.header(HEADER_RUNNER_TOKEN);

            if (!readTokenOrAlias) {
                res.sendStatus(404);
                return;
            }

            return this.tokenAliasProvider.read(readTokenOrAlias).then((token?: string) => {
                if (!token) {
                    token = readTokenOrAlias;
                }

                jwt.verify(token, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, runnerToken: IRunnerToken | undefined) => {
                    if (!errors && runnerToken) {
                        this.definitionProvider
                            .readByPublicKey(runnerToken.user, runnerToken.definition, true)
                            .then(async (definition?: IDefinition) => {
                                if (!(definition && definition.data) || runnerToken.type !== "collect") {
                                    res.sendStatus(404);
                                    return;
                                }

                                this.metricProvider.add({
                                    event: "read",
                                    subject: "definition",
                                    subjectId: definition.id,
                                });

                                res.header("Cache-Control", "no-store");
                                res.json({ definition: definition.data });
                            })
                            .catch(next);
                    } else {
                        res.sendStatus(404);
                    }
                });
            });
        };
    }

    private get styles(): express.RequestHandler {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const readTokenOrAlias = req.header(HEADER_RUNNER_TOKEN);
            if (!readTokenOrAlias) {
                res.sendStatus(404);
                return;
            }

            return this.tokenAliasProvider.read(readTokenOrAlias).then((token?: string) => {
                if (!token) {
                    token = readTokenOrAlias;
                }

                jwt.verify(token, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, runnerToken: IRunnerToken | undefined) => {
                    if (!errors && runnerToken) {
                        this.definitionProvider
                            .readByPublicKey(runnerToken.user, runnerToken.definition, false)
                            .then(async (definition?: IDefinition) => {
                                if (!definition || runnerToken.type !== "collect") {
                                    res.sendStatus(404);
                                    return;
                                }

                                res.header("Cache-Control", "no-store");
                                res.json({ styles: definition.styles });
                            })
                            .catch(next);
                    } else {
                        res.sendStatus(404);
                    }
                });
            });
        };
    }

    private get l10n(): express.RequestHandler {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const readTokenOrAlias = req.header(HEADER_RUNNER_TOKEN);
            if (!readTokenOrAlias) {
                res.sendStatus(404);
                return;
            }

            return this.tokenAliasProvider.read(readTokenOrAlias).then((token?: string) => {
                if (!token) {
                    token = readTokenOrAlias;
                }

                jwt.verify(token, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, runnerToken: IRunnerToken | undefined) => {
                    if (!errors && runnerToken) {
                        this.definitionProvider
                            .readByPublicKey(runnerToken.user, runnerToken.definition, false)
                            .then(async (definition?: IDefinition) => {
                                if (!definition || runnerToken.type !== "collect") {
                                    res.sendStatus(404);
                                    return;
                                }

                                res.header("Cache-Control", "no-store");
                                res.json({ l10n: definition.l10n });
                            })
                            .catch(next);
                    } else {
                        res.sendStatus(404);
                    }
                });
            });
        };
    }

    private get template(): express.RequestHandler {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const templateToken = (req as ITemplateTokenRequest).token;

            return this.definitionProvider
                .readByPublicKey(templateToken.user, templateToken.definition, true)
                .then(async (definition?: IDefinition) => {
                    if (!definition) {
                        res.sendStatus(404);
                        return;
                    }

                    res.header("Cache-Control", "no-store");
                    res.json({ definition: definition.data, styles: definition.styles, l10n: definition.l10n });
                })
                .catch(next);
        };
    }

    private get snapshot(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const readTokenOrAlias = req.header(HEADER_RUNNER_TOKEN);

            if (!readTokenOrAlias || readTokenOrAlias.length <= SHORT_ID_SIZE) {
                res.sendStatus(404);
                return;
            }

            return this.tokenAliasProvider.read(readTokenOrAlias).then((token?: string) => {
                if (!token) {
                    token = readTokenOrAlias;
                }

                jwt.verify(token, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, runnerToken: IRunnerToken | undefined) => {
                    if (!errors && runnerToken) {
                        if (runnerToken.snapshot && runnerToken.type === "collect") {
                            return this.snapshotProvider
                                .read(runnerToken.user, runnerToken.definition, runnerToken.snapshot, true)
                                .then((snapshot?: ISnapshot) => {
                                    res.header("Cache-Control", "no-store");

                                    if (snapshot && snapshot.data) {
                                        this.metricProvider.add({
                                            event: "read",
                                            subject: "snapshot",
                                            subjectId: snapshot.id,
                                        });

                                        return res.json(snapshot.data);
                                    } else {
                                        return res.json(undefined);
                                    }
                                })
                                .catch(next);
                        }
                    }

                    return res.json(undefined);
                });
            });
        };
    }

    private get postSnapshot(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const readTokenOrAlias = req.header(HEADER_RUNNER_TOKEN);

            if (!readTokenOrAlias) {
                res.sendStatus(400);
                return;
            }

            return this.tokenAliasProvider.read(readTokenOrAlias).then((token?: string) => {
                if (!token) {
                    token = readTokenOrAlias;
                }

                jwt.verify(token, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, runnerToken: IRunnerToken | undefined) => {
                    if (!errors && runnerToken) {
                        const emailAddress = castToString(req.body.emailAddress);
                        const snapshotData = req.body.snapshot || undefined;
                        const runner = castToString(req.body.runner);
                        const language = castToString(req.body.language);
                        const locale = castToString(req.body.locale);

                        if (
                            !emailAddress ||
                            !snapshotData ||
                            validator.isEmail(emailAddress, {}) === false ||
                            runnerToken.type !== "collect"
                        ) {
                            res.sendStatus(400);
                            return;
                        }

                        // Don't save the user's email address for privacy reasons.
                        // Save a hash instead, to be able to update the snapshot for the same user (when the user pauses again).
                        const key = createHash(emailAddress);

                        return this.snapshotProvider
                            .upsert(runnerToken.user, runnerToken.definition, key, JSON.stringify(snapshotData), runner, language, locale)
                            .then((id?: string) => {
                                if (!id) {
                                    this.logProvider.warning(
                                        `Snapshot for user ${runnerToken.user} and definition ${runnerToken.definition} failed to be saved.`
                                    );
                                    return Promise.resolve(401);
                                }

                                this.metricProvider.add({
                                    event: "create",
                                    subject: "snapshot",
                                    subjectId: id,
                                    userId: runnerToken.user,
                                    ip: req.ip,
                                    referer: getReferer(req),
                                });

                                const snapshotToken = this.tokenProvider.generateRunnerToken(runnerToken.user, runnerToken.definition, id);

                                return this.sendSnapshotEmail(emailAddress, snapshotToken).then(() => Promise.resolve(200));
                            })
                            .then((code: number) => res.sendStatus(code))
                            .catch((err: {}) => next(err));
                    }

                    return res.sendStatus(400);
                });
            });
        };
    }

    private async sendSnapshotEmail(emailAddress: string, token: string): Promise<string> {
        const link = this.createMagicLink(token);
        return this.sendMagicLink(emailAddress, link)
            .then(() => link)
            .catch(logCriticalError(this.logProvider));
    }

    private createMagicLink(token: string): string {
        return `${URL}${RUN}?token=${token}`;
    }

    private async sendMagicLink(emailTo: string, link: string): Promise<void> {
        return ejs
            .renderFile(path.join(__dirname, "../../../views/emails/snapshot.ejs"), { link, recipient: emailTo, url: URL })
            .then((html: string) => {
                return this.emailServiceProvider.send({
                    to: emailTo,
                    from: {
                        name: EMAIL_FROM_NAME,
                        email: EMAIL_FROM_ADDRESS,
                    },
                    subject: "Resume your form with this magic link",
                    html,
                });
            });
    }

    private get announceResponse(): express.RequestHandler {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const requestFingerprint = castToString(req.body.fingerprint);
            const requestChecksum = castToString(req.body.checksum);
            const runner = castToString(req.body.runner);
            const readTokenOrAlias = req.header(HEADER_RUNNER_TOKEN);

            if (!requestFingerprint || !requestChecksum || !readTokenOrAlias) {
                res.sendStatus(400);

                return;
            }

            return this.tokenAliasProvider.read(readTokenOrAlias).then((token?: string) => {
                token = token || readTokenOrAlias;

                jwt.verify(token, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, runnerToken: IRunnerToken | undefined) => {
                    if (!errors && runnerToken && runnerToken.type === "collect") {
                        return this.responseProvider
                            .announce(runnerToken.user, runnerToken.definition, requestFingerprint, requestChecksum, runner, req.ip)
                            .then((announcement) => {
                                if (announcement) {
                                    res.json(announcement);
                                } else {
                                    this.logProvider.warning(
                                        `Response announcement for user ${runnerToken.user} and definition ${runnerToken.definition} failed to be created.`
                                    );

                                    res.sendStatus(401);
                                }
                            })
                            .catch(next);
                    }

                    return res.sendStatus(400);
                });
            });
        };
    }

    private get submitResponse(): express.RequestHandler {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            const requestId = castToString(req.body.id);
            const requestNonce = castToString(req.body.nonce);
            const requestPowHashRate = castToFloat(req.body.powHashRate);
            const requestPowDuration = castToNumber(req.body.powDuration);
            const requestExportables = req.body.exportables as Export.IExportables | undefined;
            const requestActionables = req.body.actionables as Export.IActionables | undefined;
            const readTokenOrAlias = req.header(HEADER_RUNNER_TOKEN);

            if (
                !requestId ||
                !requestNonce ||
                !requestExportables ||
                !requestExportables.fingerprint ||
                !requestExportables.fields ||
                !readTokenOrAlias
            ) {
                res.sendStatus(400);

                return;
            }

            return this.tokenAliasProvider.read(readTokenOrAlias).then((token?: string) => {
                token = token || readTokenOrAlias;

                jwt.verify(token, TRIPETTO_APP_PK, (errors: jwt.VerifyErrors, runnerToken: IRunnerToken | undefined) => {
                    if (!errors && runnerToken && runnerToken.type === "collect") {
                        const language = castToString(req.body.language);
                        const locale = castToString(req.body.locale);

                        return this.responseProvider
                            .submit(
                                runnerToken.user,
                                runnerToken.definition,
                                requestId,
                                requestNonce,
                                requestPowHashRate,
                                requestPowDuration,
                                language,
                                locale,
                                requestExportables,
                                requestActionables
                            )
                            .then((result) => {
                                if (result && result.id) {
                                    this.metricProvider.add({
                                        event: "create",
                                        subject: "response",
                                        userId: runnerToken.user,
                                        ip: req.ip,
                                        referer: getReferer(req),
                                    });

                                    if (runnerToken.snapshot) {
                                        // Delete snapshot, don't wait for it.
                                        this.snapshotProvider
                                            .delete(runnerToken.user, runnerToken.definition, runnerToken.snapshot)
                                            .catch((err: {}) => this.logProvider.warning(err));
                                    }

                                    if (requestActionables) {
                                        this.actions(requestActionables, requestExportables, locale);
                                    }

                                    this.definitionProvider
                                        .readByPublicKey(runnerToken.user, runnerToken.definition, false)
                                        .then((definition?: IDefinition) => {
                                            if (definition) {
                                                this.hooks(requestExportables, definition, result.id, result.locale);
                                            }
                                        });

                                    res.send(result.id);
                                } else {
                                    this.logProvider.warning(
                                        `Response for user ${runnerToken.user} and definition ${runnerToken.definition} failed to be saved.`
                                    );

                                    res.sendStatus(401);
                                }
                            })
                            .catch(next);
                    }

                    return res.sendStatus(400);
                });
            });
        };
    }

    private actions(actionables: Export.IActionables, exportables: Export.IExportables, locale: string): void {
        actionables.nodes.forEach((node) => {
            switch (node.type) {
                case "tripetto-block-mailer":
                    const recipient = node.data.find((data) => data.slot === "recipient")?.string || "";
                    const subject = node.data.find((data) => data.slot === "subject")?.string || "";
                    const message = node.data.find((data) => data.slot === "message")?.string || "-";
                    const sender = node.data.find((data) => data.slot === "sender")?.string || "";
                    const includeExportables = node.data.find((data) => data.slot === "data")?.value === true;

                    if (recipient && subject) {
                        this.mailer({
                            locale,
                            recipient,
                            subject,
                            message,
                            sender,
                            exportables: (includeExportables && exportables) || undefined,
                        }).catch(this.logProvider.error);
                    }
                    break;
            }
        });
    }

    private async mailer(props: {
        locale: string;
        recipient: string;
        subject: string;
        message: string;
        sender?: string;
        exportables?: Export.IExportables;
    }): Promise<void> {
        if (!validator.isEmail(props.recipient)) {
            return;
        }
        return ejs
            .renderFile(path.join(__dirname, "../../../views/emails/mailer.ejs"), {
                message: Str.CRLFToHTML(Str.makeHTMLSafe(props.message)),
                recipient: props.recipient,
                url: URL,
                fields:
                    (props.exportables &&
                        props.exportables.fields
                            .map((field) => ({
                                name: Str.CRLFToHTML(Str.makeHTMLSafe(field.name)),
                                value: Str.CRLFToHTML(
                                    Str.makeHTMLSafe(
                                        isDate(field)
                                            ? formatDate(field, "full", props.locale)
                                            : isAttachment(field)
                                            ? attachmentUrl(field)
                                            : field.string
                                    )
                                ),
                            }))
                            .filter((field: { name: string; value: string }) => (field.name && field.value ? true : false))) ||
                    [],
            })
            .then((html: string) => {
                this.emailServiceProvider.send({
                    from: {
                        email: EMAIL_FROM_ADDRESS,
                        name: EMAIL_FROM_NAME,
                    },
                    reply_to:
                        (props.sender &&
                            validator.isEmail(props.sender) && {
                                email: props.sender,
                            }) ||
                        undefined,
                    subject: props.subject,
                    to: props.recipient,
                    html,
                });
            });
    }

    private hooks(exportables: Export.IExportables, definition: IDefinition, id: string, locale: string): void {
        const hooks = definition.hooks;

        if (hooks) {
            const definitionUrl = `${URL}${RUN}/${definition.readTokenAlias}`;
            const definitionName = definition.name || "Unnamed";

            Promise.all([
                hooks.email ? this.emailHook(hooks.email, definitionName, definitionUrl, exportables, locale) : Promise.resolve(),
                hooks.slack ? this.slackHook(hooks.slack, definitionName, definitionUrl, exportables, locale) : Promise.resolve(),
                hooks.webhook ? this.webhook(hooks.webhook, exportables, id) : Promise.resolve(),
            ]).catch((error: Error) => this.logProvider.error(error));
        }
    }

    private async emailHook(
        settings: IEmailHookSettings,
        definitionName: string,
        definitionUrl: string,
        exportables: Export.IExportables,
        locale: string
    ): Promise<void> {
        if (settings.enabled && settings.recipient) {
            const recipient = settings.recipient;

            return ejs
                .renderFile(path.join(__dirname, "../../../views/emails/response.ejs"), {
                    definitionName,
                    definitionUrl,
                    recipient,
                    url: URL,
                    fields: settings.includeFields
                        ? exportables.fields
                              .map((field) => ({
                                  name: Str.CRLFToHTML(Str.makeHTMLSafe(field.name)),
                                  value: Str.CRLFToHTML(
                                      Str.makeHTMLSafe(
                                          isDate(field)
                                              ? formatDate(field, "full", locale)
                                              : isAttachment(field)
                                              ? attachmentUrl(field)
                                              : field.string
                                      )
                                  ),
                              }))
                              .filter((field: { name: string; value: string }) => (field.name && field.value ? true : false))
                        : [],
                })
                .then((html: string) => {
                    const emailOptions = {
                        to: recipient,
                        from: { name: EMAIL_FROM_NAME, email: EMAIL_FROM_ADDRESS },
                        subject: `New submission for form ${definitionName}`,
                        html,
                    };

                    return this.emailServiceProvider.send(emailOptions);
                });
        }
    }

    private postMessage(url: string, data: {}): superagent.SuperAgentRequest {
        return superagent.post(url).set("Accept", "application/json").send(data);
    }

    private createSlackAttachment(exportables: Export.IExportables, locale: string): {} {
        const fields: { title: string; value: string; short: boolean }[] = [];

        exportables.fields.forEach((field) => {
            const value = isDate(field) ? formatDate(field, "full", locale) : isAttachment(field) ? attachmentUrl(field) : field.string;

            if (value) {
                fields.push({ title: field.name, value, short: false });
            }
        });

        return {
            fields,
        };
    }

    private slackHook(
        settings: ISlackHookSettings,
        definitionName: string,
        definitionUrl: string,
        exportables: Export.IExportables,
        locale: string
    ): void {
        if (settings.enabled && settings.url) {
            const data = {
                text: `New submission for <${definitionUrl}|${definitionName}>`,
                username: "Tripetto",
                icon_url: URL + "/assets/mail-logo-tripetto.png",
                attachments: settings.includeFields ? [this.createSlackAttachment(exportables, locale)] : [],
            };

            this.postMessage(settings.url, data).end();
        }
    }

    private get testSlack(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            if (!req.body || typeof req.body.url !== "string" || !req.body.url || !validator.isURL(req.body.url)) {
                res.sendStatus(400);
            }

            this.postMessage(req.body.url, {
                text: "This is a test message from Tripetto. Your Slack configuration works ✔",
                username: "Tripetto",
                icon_url: URL + "/assets/mail-logo-tripetto.png",
                attachments: req.body.includeFields
                    ? [
                          {
                              fields: [{ title: "Test", value: "Hello World!", short: false }],
                          },
                      ]
                    : undefined,
            })
                .then((result) => {
                    res.sendStatus(result.status);
                })
                .catch(() => {
                    res.sendStatus(400);
                });
        };
    }

    private webhook(settings: IWebHookSettings, exportables: Export.IExportables, id: string): void {
        if (settings.enabled && settings.url) {
            if (settings.nvp) {
                const data: {
                    [key: string]: string;
                } = { tripettoId: id, tripettoCreateDate: new Date().toISOString(), tripettoFingerprint: exportables.fingerprint };

                exportables.fields.forEach((field) => {
                    if (field.name) {
                        let name = field.name;
                        let counter = 1;

                        while (isString(data[name])) {
                            name = `${field.name} (${++counter})`;
                        }

                        data[name] = isAttachment(field) ? attachmentUrl(field) : field.string;
                    }
                });

                this.postMessage(settings.url, data).end();
            } else {
                const data = { id, created: new Date().toISOString(), ...exportables };

                this.postMessage(settings.url, data).end();
            }
        }
    }

    private get testWebhook(): express.RequestHandler {
        return (req: express.Request, res: express.Response) => {
            if (!req.body || typeof req.body.url !== "string" || !req.body.url || !validator.isURL(req.body.url)) {
                res.sendStatus(400);
            }

            this.postMessage(
                req.body.url,
                req.body.nvp
                    ? {
                          test: "Hello World!",
                      }
                    : {
                          fingerprint: "d260fcb7a07abf68233107f11a4791956a939e41854e4db31c1930286d83726a",
                          fields: [
                              {
                                  name: "Test",
                                  string: "Hello World!",
                                  value: "Hello World!",
                                  time: 1593696198103,
                                  key: "4271169a754c5d1ea98813267cf80ab675a7b28df5c7509dd39ef9d794d2e7f4",
                              },
                          ],
                      }
            )
                .then((result) => {
                    res.sendStatus(result.status);
                })
                .catch(() => {
                    res.sendStatus(400);
                });
        };
    }
}
