import "reflect-metadata";
import { Server } from "./server";
import { dependencyContainer } from "./dependencies";
import {
    AuthenticationController,
    ExportController,
    FileController,
    GraphqlController,
    L10nController,
    MetricController,
    RootController,
    RunnerController,
} from "./controllers";
import { logProviderSymbol } from "./symbols";
import { ENV, SENTRY_DSN_SERVER } from "./settings";
import * as Sentry from "@sentry/node";

if (SENTRY_DSN_SERVER) {
    console.log(`Logging to Sentry using DSN: ${SENTRY_DSN_SERVER}`);

    Sentry.init({ dsn: SENTRY_DSN_SERVER, environment: ENV });
}

if (ENV === "development") {
    // Print out a stack trace on process warnings.
    process.on("warning", (warning: Error) => console.warn(warning.stack));
}

Server.start(
    [
        dependencyContainer.resolve(RootController),
        dependencyContainer.resolve(AuthenticationController),
        dependencyContainer.resolve(RunnerController),
        dependencyContainer.resolve(FileController),
        dependencyContainer.resolve(GraphqlController),
        dependencyContainer.resolve(ExportController),
        dependencyContainer.resolve(MetricController),
        dependencyContainer.resolve(L10nController),
    ],
    dependencyContainer.get(logProviderSymbol),
    process.env.PORT
);
