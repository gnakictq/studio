import { Field, InputType } from "type-graphql";
import { definition } from "./scalar";
import { IDefinition as ITripettoDefinition } from "tripetto";
import { IServerDefinitionInput } from "./interface";

@InputType({ description: "Describes the input of a definition." })
export class DefinitionInput implements IServerDefinitionInput {
    @Field({ nullable: true })
    id!: string;

    @Field({ description: "Name of definition." })
    name!: string;

    @Field(() => definition, { description: "Content of a definition." })
    data!: ITripettoDefinition;
}
