import { IUser } from "../users";
import { IHookSettings, IServerDefinition } from "./interface";
import { TRunners } from "./runners";
import { TL10n, TStyles } from "tripetto-runner-foundation";
import { Definition, DefinitionInput } from ".";
import { MutationResult } from "../mutationresult";
import { inject, injectable } from "inversify";
import {
    definitionProviderSymbol,
    logProviderSymbol,
    metricProviderSymbol,
    responseProviderSymbol,
    shortIdProviderSymbol,
    tokenAliasProviderSymbol,
    tokenProviderSymbol,
    userProviderSymbol,
} from "../../symbols";
import { Arg, Authorized, Ctx, Info, Mutation, Query, Resolver } from "type-graphql";
import { IUserContext } from "../tokens/interface";
import { CreateResult } from "../mutationresult/type";
import { GraphQLResolveInfo } from "graphql";
import { isDataListedInQuery } from "../helpers/query";
import {
    IDefinitionProvider,
    ILogProvider,
    IMetricProvider,
    IResponseProvider,
    IShortIdProvider,
    ITokenAliasProvider,
    ITokenProvider,
    IUserProvider,
} from "../../providers";
import { logError } from "../../helpers/error";
import { ITokenAlias } from "../../providers/token-alias";
import { hooks, l10n, runners, styles } from "./scalar";

@injectable()
@Resolver()
export class DefinitionResolver {
    private readonly definitionProvider: IDefinitionProvider;
    private readonly userProvider: IUserProvider;
    private readonly responseProvider: IResponseProvider;
    private readonly tokenProvider: ITokenProvider;
    private readonly tokenAliasProvider: ITokenAliasProvider;
    private readonly shortIdProvider: IShortIdProvider;
    private readonly logProvider: ILogProvider;
    private readonly metricProvider: IMetricProvider;

    constructor(
        @inject(definitionProviderSymbol) definitionProvider: IDefinitionProvider,
        @inject(userProviderSymbol) userProvider: IUserProvider,
        @inject(responseProviderSymbol) responseProvider: IResponseProvider,
        @inject(tokenProviderSymbol) tokenProvider: ITokenProvider,
        @inject(tokenAliasProviderSymbol) tokenAliasProvider: ITokenAliasProvider,
        @inject(shortIdProviderSymbol) shortIdProvider: IShortIdProvider,
        @inject(logProviderSymbol) logProvider: ILogProvider,
        @inject(metricProviderSymbol) metricProvider: IMetricProvider
    ) {
        this.logProvider = logProvider;
        this.definitionProvider = definitionProvider;
        this.userProvider = userProvider;
        this.responseProvider = responseProvider;
        this.tokenProvider = tokenProvider;
        this.tokenAliasProvider = tokenAliasProvider;
        this.shortIdProvider = shortIdProvider;
        this.metricProvider = metricProvider;
    }

    private async updateTokenAlias(userId: string, user?: IUser, definition?: IServerDefinition): Promise<ITokenAlias | undefined> {
        if (!user || !definition) {
            return undefined;
        }

        const alias = this.shortIdProvider.generate();
        const readToken = this.tokenProvider.generateRunnerToken(user.publicKey, definition.publicKey);
        const tokenAlias: ITokenAlias = { token: readToken, alias: alias };

        return Promise.all([
            this.definitionProvider.updateReadTokenAlias(userId, definition.id, alias),
            this.tokenAliasProvider.create(tokenAlias),
        ]).then(() => tokenAlias);
    }

    private async readTokenByAlias(alias: string): Promise<ITokenAlias | undefined> {
        return this.tokenAliasProvider.read(alias).then((token?: string) => {
            return token ? { token, alias } : undefined;
        });
    }

    private async updateTemplateTokenAlias(userId: string, user?: IUser, definition?: IServerDefinition): Promise<ITokenAlias | undefined> {
        if (!user || !definition) {
            return undefined;
        }

        const alias = this.shortIdProvider.generate();
        const templateToken = this.tokenProvider.generateTemplateToken(user.publicKey, definition.publicKey);
        const tokenAlias: ITokenAlias = { token: templateToken, alias: alias };

        return Promise.all([
            this.definitionProvider.updateTemplateTokenAlias(userId, definition.id, alias),
            this.tokenAliasProvider.create(tokenAlias),
        ]).then(() => tokenAlias);
    }

    @Authorized()
    @Query(() => Definition, { description: "Read definition for authenticated user." })
    async definition(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Ctx() context: IUserContext,
        @Info() info: GraphQLResolveInfo
    ): Promise<Definition | undefined> {
        return Promise.all([
            this.definitionProvider.readById(context.token.user, id, isDataListedInQuery(info)),
            this.userProvider.readById(context.token.user),
            this.responseProvider.readCountByDefinition(context.token.user, id),
        ])

            .then(([definition, user, responseCount]: [IServerDefinition | undefined, IUser | undefined, number | undefined]) => {
                if (!definition || !user) {
                    return Promise.resolve(undefined);
                }

                const result = new Definition().load(definition);
                result.responseCount = responseCount || 0;

                return Promise.all([
                    !definition.readTokenAlias
                        ? this.updateTokenAlias(context.token.user, user, definition)
                        : this.readTokenByAlias(definition.readTokenAlias),
                    !definition.templateTokenAlias
                        ? this.updateTemplateTokenAlias(context.token.user, user, definition)
                        : Promise.resolve(undefined),
                ]).then(([readTokenAliasPair, templateTokenAliasPair]: [ITokenAlias | undefined, ITokenAlias | undefined]) => {
                    if (readTokenAliasPair) {
                        result.readToken = readTokenAliasPair.token;
                        result.readTokenAlias = readTokenAliasPair.alias;
                    }
                    if (templateTokenAliasPair) {
                        result.templateTokenAlias = templateTokenAliasPair.alias;
                    }

                    return result;
                });
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => CreateResult, { description: "Create definition for authenticated user." })
    async createDefinition(
        @Arg("runner", () => runners, { description: "New runner type." }) runner: TRunners,
        @Ctx() context: IUserContext
    ): Promise<CreateResult> {
        return Promise.all([this.definitionProvider.create(context.token.user, runner), this.userProvider.readById(context.token.user)])
            .then(async ([definition, user]: [IServerDefinition | undefined, IUser | undefined]) => {
                return Promise.all([
                    this.updateTokenAlias(context.token.user, user, definition),
                    this.updateTemplateTokenAlias(context.token.user, user, definition),
                ]).then(([readTokenAliasPair, templateTokenAliasPair]: [ITokenAlias | undefined, ITokenAlias | undefined]) => {
                    if (definition && readTokenAliasPair) {
                        definition.readToken = readTokenAliasPair.token;
                        definition.readTokenAlias = readTokenAliasPair.alias;
                    }
                    if (definition && templateTokenAliasPair) {
                        definition.templateTokenAlias = templateTokenAliasPair.alias;
                    }
                    return definition;
                });
            })
            .then((definition?: IServerDefinition) => {
                const result = new CreateResult();
                if (definition) {
                    this.metricProvider.add({
                        event: "create",
                        subject: "definition",
                        subjectId: definition.id,
                        userId: context.token.user,
                    });
                    result.id = definition.id;
                }
                return result;
            })
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => Definition, { description: "Update definition for authenticated user." })
    async updateDefinition(
        @Arg("data", { description: "Definition to update." }) input: DefinitionInput,
        @Ctx() context: IUserContext
    ): Promise<Definition> {
        return this.definitionProvider
            .update(context.token.user, input)
            .then(() => new Definition().load(input))
            .catch(logError(this.logProvider));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update runner for authenticated user." })
    async updateRunner(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("runner", () => runners, { description: "New runner type." }) input: TRunners,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateRunner(context.token.user, id, input)
            .then(() => ({ isSuccess: true }))
            .catch(logError(this.logProvider))
            .catch(() => ({ isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update styles for authenticated user." })
    async updateStyles(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("styles", () => styles, { description: "Style to update." }) input: TStyles,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateStyles(context.token.user, id, input)
            .then(() => ({ isSuccess: true }))
            .catch(logError(this.logProvider))
            .catch(() => ({ isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update l10n data for authenticated user." })
    async updateL10n(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("l10n", () => l10n, { description: "L10n data to update." }) input: TL10n,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateL10n(context.token.user, id, input)
            .then(() => ({ isSuccess: true }))
            .catch(logError(this.logProvider))
            .catch(() => ({ isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Update hooks for authenticated user." })
    async updateHooks(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Arg("hooks", () => hooks, { description: "Hooks to update." }) input: IHookSettings,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        return this.definitionProvider
            .updateHooks(context.token.user, id, input)
            .then(() => ({ isSuccess: true }))
            .catch(logError(this.logProvider))
            .catch(() => ({ isSuccess: false }));
    }

    @Authorized()
    @Mutation(() => MutationResult, { description: "Delete definition for authenticated user." })
    async deleteDefinition(
        @Arg("id", { description: "Id of definition." }) id: string,
        @Ctx() context: IUserContext
    ): Promise<MutationResult> {
        this.metricProvider.add({
            event: "delete",
            subject: "definition",
            subjectId: id,
            userId: context.token.user,
        });

        return this.definitionProvider
            .delete(context.token.user, id)
            .then(() => ({ isSuccess: true }))
            .catch(logError(this.logProvider));
    }
}
