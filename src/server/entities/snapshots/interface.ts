import { ISnapshot as ISnapshotData } from "tripetto-runner-foundation";

export interface ISnapshot {
    id: string;
    key: string;
    data?: ISnapshotData;
    created: number;
    modified: number;
}
