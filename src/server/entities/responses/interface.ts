export interface IResponseAnnouncement {
    readonly id: string;
    readonly difficulty: number;
    readonly timestamp: number;
}

export interface IResponseSubmission {
    readonly id: string;
    readonly language: string;
    readonly locale: string;
}

export interface IResponse {
    readonly id: string;
    readonly created: number;
    readonly fingerprint: string;
    readonly stencil: string;
    readonly data?: string;
    readonly attachments?: string[];
}
