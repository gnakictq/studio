import { Field, InputType } from "type-graphql";
import { IWorkspaceInput, TWorkspace } from "./interface";
import { workspace } from "./scalar";

@InputType({ description: "Describes a workspace." })
export class WorkspaceInput implements IWorkspaceInput {
    @Field({ nullable: true, description: "Id of workspace." })
    id!: string;

    @Field({ description: "Name of workspace." })
    name!: string;

    @Field({ description: "Specifies the number of tiles in the workspace." })
    tileCount!: number;

    @Field(() => workspace, { description: "Data of workspace." })
    data!: TWorkspace;
}
