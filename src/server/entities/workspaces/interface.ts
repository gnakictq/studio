import { Components } from "tripetto";

export type TWorkspace = Components.IWorkspace;

export interface IWorkspaceInput {
    id: string;
    name: string;
    tileCount: number;
    data?: TWorkspace;
}

export interface IWorkspace extends IWorkspaceInput {
    created: number;
    modified: number;
}
