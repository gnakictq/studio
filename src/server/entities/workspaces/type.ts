import { IWorkspace, IWorkspaceInput } from "./interface";
import { Field, ObjectType } from "type-graphql";
import { Loadable } from "../loadable";
import { workspace } from "./scalar";
import { TWorkspace } from "./interface";

@ObjectType({ description: "Describes a workspace." })
export class Workspace extends Loadable<IWorkspaceInput> implements IWorkspace {
    @Field({ description: "Id of workspace." })
    id!: string;

    @Field({ nullable: true, description: "Name of workspace." })
    name!: string;

    @Field({ description: "Specifies the number of tiles in the workspace." })
    tileCount!: number;

    @Field(() => workspace, { nullable: true, description: "Data of workspace." })
    data!: TWorkspace;

    @Field({ nullable: true, description: "Timestamp when workspace was created." })
    created!: number;

    @Field({ nullable: true, description: "Timestamp when workspace was last modified." })
    modified!: number;
}
